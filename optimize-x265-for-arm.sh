#!/bin/bash

set -euo pipefail

parent=/root
build_dir="${parent}/x265-optimized"
dpkg_version_file="${parent}/x265-dmo-version.txt"

if [[ -f "$dpkg_version_file" ]]
then
    existing_x265_version="$(<${dpkg_version_file})"
else
    existing_x265_version=
fi

new_x265_version="$(dpkg -s libx265-dev | grep '^Version: ')"
new_x265_version="${new_x265_version#Version: }"

if [[ "$existing_x265_version" == "$new_x265_version" ]]
then
    exit 0
fi

subbed_x265_version="${new_x265_version}ag"

echo "Rebuilding x265-dmo as libx265-dev version has changed from [${existing_x265_version}] to [${new_x265_version}]."
rm -rf "$build_dir"
mkdir -p "$build_dir"
cd "$build_dir"
wget -O apple_arm64_x265.patch 'https://raw.githubusercontent.com/Vargol/ffmpeg-apple-arm64-build/master/build/apple_arm64_x265.patch'
echo "786a0e2d3720c33cd7cd5aeaed3057cdc07b65ea6f053f8ba8eeea5b5538a908	apple_arm64_x265.patch" > apple_arm64_x265.patch.sha256
sha256sum -c apple_arm64_x265.patch.sha256
DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND
apt source x265-dmo
cd x265-dmo-*
quilt import ../apple_arm64_x265.patch
quilt push -a
quilt new fix_arm64_x265.patch
quilt add source/common/arm64/dct-prim.cpp
quilt add source/CMakeLists.txt
quilt add source/common/CMakeLists.txt
quilt add source/dynamicHDR10/CMakeLists.txt
sed -ri 's/OPTIONS =(.*)$/OPTIONS =\1 -DCMAKE_BUILD_TYPE=Release/; /-DENABLE_ASSEMBLY=OFF/d; /dh_auto_configure.+--builddirectory=build\/linux\/10bit/{n;s/\\$/-DMAIN10=ON -DENABLE_HDR10_PLUS=ON \\/}' debian/rules
sed -ri 's/#pragma[[:space:]]+unroll/#pragma GCC unroll/g' source/common/arm64/dct-prim.cpp
sed -ri '/mstackrealign/{N;d;d;d}' source/CMakeLists.txt source/dynamicHDR10/CMakeLists.txt
sed -ri '/\<if\(ENABLE_ASSEMBLY AND.+ARM/a \ \ \ \ add_compile_options(-flax-vector-conversions)' source/common/CMakeLists.txt
find debian -type f -print0 | xargs -0 sed -ri "s/${new_x265_version}/${subbed_x265_version}/g"
quilt refresh
quilt pop -a

CMAKE_BUILD_PARALLEL_LEVEL="$(nproc)"
export CMAKE_BUILD_PARALLEL_LEVEL
dpkg-buildpackage -us -uc
cd -
shopt -s extglob
dpkg -i libx265-+([0-9])_*deb libx265-dev_*deb x265_*deb

echo "$subbed_x265_version" > "$dpkg_version_file"
