#!/bin/bash

set -euo pipefail

now="$(date +%s)"
for d in /srv/data/frigate/recordings/2*
do
    b="$(basename "$d")"
    t="$(date +%s -d "$b")"
    if [[ $(($t + 2678400)) -le $now ]]
    then
        rm -rf "$d"
    fi
done

find /srv/data/frigate/recordings -mtime +7 -name '*.mp4' '!' -name '*.hevc.mp4' '!' -name '*.avc.mp4' |
    while read -r recording
    do
        avcsize="$(stat --format=%s "$recording")"
        ffmpeg -y -hwaccel qsv -qsv_device /dev/dri/renderD128 -init_hw_device qsv=hw -hwaccel_output_format qsv -i "$recording" -c:v hevc_qsv -vf hwupload -preset veryslow -global_quality:v 22 -rdo 1 -adaptive_i 1 -adaptive_b 1 -extbrc 1 -look_ahead_depth 40 -g 256 -b_strategy 1 -bf 7 -refs 5 -c:a copy "${recording%.mp4}.hevc.mp4" < /dev/null
        hevcsize="$(stat --format=%s "${recording%.mp4}.hevc.mp4")"
        if [[ "$hevcsize" -lt "$avcsize" ]]
        then
            rm "$recording"
        else
            rm "${recording%.mp4}.hevc.mp4"
            mv "$recording" "${recording%.mp4}.avc.mp4"
        fi
    done
