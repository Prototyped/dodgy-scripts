#!/usr/bin/env python3

from argparse import ArgumentParser
import json
import os
from pathlib import Path
import re
import subprocess
import sys
from typing import List
from urllib.request import urlcleanup, urlopen, Request


def parse_args(prog: str, *args: str) -> object:
    parser = ArgumentParser(prog=prog, description='Container upgrader')
    parser.add_argument('--repo', action='store', required=True, help='Repository')
    parser.add_argument('--tag', action='store', required=True, help='Image tag')
    parser.add_argument('--rebuild', action='store', help='Path to local directory containing Dockerfile to rebuild')
    parser.add_argument('--local-image', action='store', help='Name of local image name to build from Dockerfile')
    parser.add_argument('--build-arg', action='append', help='Argument to pass to the Docker build.')
    parsed_args = parser.parse_args(*args)

    if (not parsed_args.rebuild) != (not parsed_args.local_image):
        raise ValueError('--rebuild --local-image must both be specified.')

    if parsed_args.build_arg and not parsed_args.rebuild:
        raise ValueError('--build-arg must be specified only with --rebuild.')

    return parsed_args

def get_installed_digest(repo: str, tag: str) -> str:
    content = subprocess.check_output([
        'docker', 'image', 'inspect', f'{repo}:{tag}',
    ], stderr=subprocess.STDOUT).decode('UTF-8')
    obj = json.loads(content)
    return obj[0]['Id']


def pull_container(repo: str, tag: str):
    try:
        subprocess.check_output([
            'docker', 'pull', f'{repo}:{tag}',
        ], stderr=subprocess.STDOUT).decode('UTF-8')
    except subprocess.CalledProcessError as exc:
        print(exc.output.decode('UTF-8'))
        raise


def container_updated(repo: str, tag: str) -> int:
    try:
        orig = get_installed_digest(repo, tag)
    except subprocess.CalledProcessError as exc:
        orig = None

    pull_container(repo, tag)
    new = get_installed_digest(repo, tag)
    return 0 if not orig or orig != new else 1


def rebuild_container(repo_src_dir: Path, repo: str, tag: str, my_repo: str, build_arg: List[str]) -> int:
    if container_updated(repo, tag) == 1:
        return 1

    if build_arg:
        build_args=[f'--build-arg={arg}' for arg in build_arg]
    else:
        build_args=[]

    try:
        print(subprocess.check_output([
            'docker', 'pull', f'{repo}:{tag}',
        ], stderr=subprocess.STDOUT).decode('UTF-8'))
    except subprocess.CalledProcessError as exc:
        print(exc.output.decode('UTF-8'))
        raise

    try:
        print(subprocess.check_output([
            'docker', 'build', *build_args, '-t', f'{my_repo}:latest', '.',
        ], stderr=subprocess.STDOUT, cwd=repo_src_dir).decode('UTF-8'))
    except subprocess.CalledProcessError as exc:
        print(exc.output.decode('UTF-8'))
        raise

    return 0


if __name__ == '__main__':
    parsed_args = parse_args(sys.argv[0], sys.argv[1:])
    if parsed_args.rebuild:
        sys.exit(
            rebuild_container(
                parsed_args.rebuild,
                parsed_args.repo,
                parsed_args.tag,
                parsed_args.local_image,
                parsed_args.build_arg))
    else:
        sys.exit(
            container_updated(
                parsed_args.repo,
                parsed_args.tag))

