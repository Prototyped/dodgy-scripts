'Hibernate the computer if no users are currently logged in.
strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process WHERE Name='explorer.exe'")
If colProcessList.count = 0 Then
    Dim WshShell : set WshShell = CreateObject("WScript.Shell")
    WshShell.Run "c:\windows\system32\shutdown.exe /h"
End If