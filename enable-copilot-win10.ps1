# Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
	if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
		$CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
		Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
		Exit
	}
}

c:\util\vivetool.exe /enable /id:46686174,47530616,44755019

$regPath = "HKCU:\\SOFTWARE\\Microsoft\\Windows\\Shell\\Copilot\\BingChat"
$regName = "IsUserEligible"

Set-ItemProperty -Path $regPath -Name $regName -Value 1
