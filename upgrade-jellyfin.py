#!/usr/bin/env python3

import json
import os
from pathlib import Path
import re
import subprocess
import sys
from urllib.request import urlcleanup, urlopen, Request


def get_installed_digest(repo: str, tag: str) -> str:
    content = subprocess.check_output([
        'docker', 'image', 'inspect', f'ghcr.io/{repo}:{tag}',
    ], stderr=subprocess.STDOUT).decode('UTF-8')
    obj = json.loads(content)
    return obj[0]['Id']


def get_upstream_digest(repo: str, tag: str) -> str:
    auth_url = f'https://ghcr.io/token?service=ghcr.io&scope=repository:{repo}:pull'
    with urlopen(auth_url) as auth_response:
        if auth_response.status < 200 or auth_response.status >= 300:
            raise RuntimeError(f'Unexpected status {auth_response.status} attempting to obtain bearer token for repository {repo}')

        token_result = auth_response.read().decode('UTF-8')
        token = json.loads(token_result)['token']

    manifest_url = f'https://ghcr.io/v2/{repo}/manifests/{tag}'
    manifest_request = Request(manifest_url, headers={
        'Authorization': f'Bearer {token}',
    })
    with urlopen(manifest_request) as manifest_response:
        if manifest_response.status < 200 or manifest_response.status >= 300:
            raise RuntimeError(f'Unexpected status {manifest_response.status} attempting to obtain manifest for repository {repo}')

        manifest_result = manifest_response.read().decode('UTF-8')
        manifest_json = json.loads(manifest_result)
        config_digest = [manifest['digest'] for manifest in manifest_json['manifests'] if manifest['platform']['architecture'] == 'amd64' and manifest['platform']['os'] == 'linux'][0]

    config_url = f'https://ghcr.io/v2/{repo}/manifests/{config_digest}'
    config_request = Request(config_url, headers={
        'Authorization': f'Bearer {token}',
    })
    with urlopen(config_request) as config_response:
        if config_response.status < 200 or config_response.status >= 300:
            raise RuntimeError(f'Unexpected status {config_response.status} attempting to obtain manifest for repository {repo}')

        config_result = config_response.read().decode('UTF-8')
        config_json = json.loads(config_result)

    return config_json['config']['digest']


def should_pull(repo: str, tag: str) -> bool:
    return get_installed_digest(repo, tag) != get_upstream_digest(repo, tag)


def rebuild_container(repo_src_dir: Path, repo: str, tag: str, my_repo: str) -> int:
    if not should_pull(repo, tag):
        return 1

    try:
        print(subprocess.check_output([
            'docker', 'pull', f'ghcr.io/{repo}:{tag}',
        ], stderr=subprocess.STDOUT).decode('UTF-8'))
    except subprocess.CalledProcessError as exc:
        print(exc.output.decode('UTF-8'))
        raise

    try:
        print(subprocess.check_output([
            'docker', 'build', '-t', f'{my_repo}:latest', '.',
        ], stderr=subprocess.STDOUT, cwd=repo_src_dir).decode('UTF-8'))
    except subprocess.CalledProcessError as exc:
        print(exc.output.decode('UTF-8'))
        raise

    return 0


if __name__ == '__main__':
    sys.exit(
        rebuild_container(
            Path(os.getenv('HOME')) / 'repos' / 'my-jellyfin',
            'linuxserver/jellyfin',
            'latest',
            'gurdasani.com/jellyfin'))
