#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long qw(GetOptionsFromArray);
use Date::Manip qw(ParseDate UnixDate);
use Time::Local qw(timegm);
use JSON;

sub usage {
    my ($message) = @_;
    print <<"EOF";
Options:
    --log               The irssi log file from which to extract quotes.
EOF

    die 'Failure: ', (defined $message ? $message : '');
}

sub parse_options {
    my ($argv) = @_;
    
    my %option_values;
    GetOptionsFromArray($argv, \%option_values, "log=s")
        or usage();
    
    if (!$option_values{log}) {
	usage('Option --log expected but not specified.');
    }

    return \%option_values;
}

sub parse_log {
    my ($log_file) = @_;
    
    open LOG, '<', $log_file
        or usage("Failed to open file $log_file: $!");

    my @quotes;
    my %context = (quotes => \@quotes, source => \*LOG);
    
    while (my $line = <LOG>) {
	chomp $line;
	handle_line(\%context, $line);
    }
    
    close LOG;
    return \@quotes;
}

sub handle_line {
    my ($context, $line) = @_;
    
    if ($line =~ /^--- (?:Log opened|Day changed) (.*)/) {
	handle_day_changed($context, $1);
    } elsif ($line =~ /^(\d{2}):(\d{2}) <([^>]+)>\s+\!q(?:uote)?\s+(a|add|d|del|delete)\s+(.*)/) {
        handle_quote_add_del($context, $1, $2, $3, $4, $5);
    } elsif ($line =~ /^(\d{2}):(\d{2}) <([^>]+)>\s+\!q(?:uote)?\s+last\s*$/) {
	handle_last($context);
    }
}

sub handle_day_changed {
    my ($context, $date) = @_;

    my $date_string = ParseDate($date);
    my ($year, $month, $day) = UnixDate($date_string, '%Y', '%m', '%d');
    $context->{year} = $year;
    $context->{month} = $month;
    $context->{day} = $day;
}

sub handle_quote_add_del {
    my ($context, $hours, $minutes, $nick, $action, $content) = @_;
    
    if ($action =~ /^a/) {
	handle_quote_add($context, $hours, $minutes, $nick, $content);
    } else {
	handle_quote_del($context, $content);
    }
}

sub handle_quote_add {
    my ($context, $hours, $minutes, $nick, $content) = @_;
    
    return if !$context->{year};
    
    # print $context->{year}, '/', $context->{month}, '/', $context->{day}, "\n";

    my $now = timegm(0, $minutes, $hours, $context->{day},
	$context->{month} - 1, $context->{year});
    
    push @{$context->{quotes}}, {
	epoch => $now,
	nick => $nick,
	content => $content,
    };
}

sub handle_quote_del {
    my ($context, $content) = @_;

    if ($content eq 'last') {
	pop @{$context->{quotes}};
    } elsif ($content =~ /^\d+$/) {
	my $offset = offset($context, $content);
	splice @{$context->{quotes}}, $offset, 1
	    if $offset >= 0;
    } else {
	print STDERR "Exception on deletion: $content\n";
    }
}

sub handle_last {
    my ($context) = @_;
    
    my $fh = $context->{source};
    while (my $line = <$fh>) {
	chomp $line;
	if ($line =~ /> \(\d+\/(\d+)\)/) {
	    $context->{last} = $1;
	    last;
	}
    }
    
    # print 'READJUST ', $context->{last}, "\n";
}

sub offset {
    my ($context, $offset) = @_;
    
    my $base = $context->{last} - scalar @{$context->{quotes}};
    # print "BASE $base OFFSET $offset\n";
    return $offset - 1 - $base;
}

my $option_values = parse_options(\@ARGV);
my $quotes = parse_log($option_values->{log});
print encode_json($quotes);
