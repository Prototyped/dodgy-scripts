#!/bin/bash

set -euo pipefail
shopt -s lastpipe

current_version() {
    dpkg-query -f '${Version}' -W alpine
}

available_version() {
    local alpine_version
    local line
    apt-cache show alpine/testing |
        while read -r line
        do
            if [[ "$line" == Version:\ * ]]
            then
                local this_version
                this_version="${line#Version: }"
                if [[ -z "${alpine_version+x}" ]] || [[ "$this_version" > "${alpine_version-x}" ]]
                then
                    alpine_version="$this_version"
                fi
            fi
        done
    echo -n "${alpine_version-}"
}

should_build() {
    local c
    local a
    a="$1"
    c="$(current_version)"
    [[ -z "${c+x}" ]] || [[ -n "${a+x}" ]] && [[ "${a-x}" > "${c-x}" ]]
}

prepare_workdir() {
    local workdir="$1"
    local available_version="$2"

    local upstream_version="${available_version%%+*}"
    
    cd "$workdir"
    rm -rf ./*

    local patch_url="https://alpineapp.email/alpine/patches/alpine-${upstream_version}/all.patch.gz"
    echo "Getting patch from $patch_url"
    curl -LSso 99_all.patch.gz "$patch_url"
    gunzip 99_all.patch.gz

    DEBIAN_FRONTEND=noninteractive
    export DEBIAN_FRONTEND
    apt -y build-dep alpine
    apt -y source alpine
}

patch_alpine() {
    local workdir="$1"
    local available_version="$2"

    local source_dir="alpine-${available_version%-*}"

    cd "$workdir/$source_dir"
    
    quilt import ../99_all.patch
    quilt push -a
    quilt refresh
    quilt pop -a

    PATH="$PATH:/usr/local/sbin:/usr/sbin:/sbin"
    export PATH
    dpkg-buildpackage -us -uc -rfakeroot
}

install_alpine() {
    local workdir="$1"
    local available_version="$2"

    local deb_built_version="${available_version%+*}"
    local upstream_version="${available_version%%+*}"

    if [[ "$deb_built_version" == "$upstream_version" ]]
    then
        deb_built_version="$available_version"
    fi

    cd "$workdir"

    if [[ -f "alpine_${available_version}_amd64.deb" ]]
    then
        deb_built_version="$available_version"
    fi
    
    dpkg -i "alpine_${deb_built_version}_amd64.deb" "alpine-doc_${deb_built_version}_all.deb" "pilot_${deb_built_version}_amd64.deb"
}

available_version="$(available_version)"

if should_build "$available_version"
then
    workdir=/home/amitg/alpine-deb
    prepare_workdir "$workdir" "$available_version"
    patch_alpine "$workdir" "$available_version"
    install_alpine "$workdir" "$available_version"
fi
