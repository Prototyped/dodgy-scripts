#!/home/amitg/v/ytmusicapi/bin/python3

import json
import os
import sys

from argparse import ArgumentParser, FileType
from ytmusicapi import YTMusic


def parse_args(*args):
    parser = ArgumentParser(
        prog='dump-yt-music-playlists.py',
        description='Dump all contents of all YouTube Music playlists in the library.')
    parser.add_argument('-c', '--credentials-file', type=FileType('r'), required=True,
                        help='Location of oauth.json (run ytmusicapi oauth to generate).')
    return parser.parse_args(args)


def playlist_ids(playlists):
    for playlist in playlists:
        yield (playlist['playlistId'], playlist['title'])


def playlist_contents(yt_music, playlist_id):
    response = yt_music.get_playlist(playlistId=playlist_id, limit=None, related=False,
                                     suggestions_limit=0)
    for track in response['tracks']:
        yield track

        
def format_track(track):
    album = track['album']
    if album is None:
        album = ''
    else:
        album = album['name']

    return f"{', '.join(map(lambda t: t['name'], track['artists']))}\t{album}\t{track['title']}\t{track['videoId']}";

        
if __name__ == '__main__':
    args = parse_args(*sys.argv[1:])
    yt_music = YTMusic(json.load(args.credentials_file))
    playlists = yt_music.get_library_playlists()

    for playlist_id, title in playlist_ids(playlists):

        playlist_dir = f"/srv/data/home/music/yt/{title}"
        os.makedirs(playlist_dir, exist_ok=True)
        os.chdir(playlist_dir)
        with open(f"{title}.txt", "wt") as playlist:
            contents = playlist_contents(yt_music, playlist_id)
            for track in contents:
                formatted_track = format_track(track)
                print(formatted_track, file=playlist)

        os.system(f"yt-dlp --cookies-from-browser edge -f bestaudio https://music.youtube.com/playlist?list={playlist_id}")
