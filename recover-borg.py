#!/usr/bin/python3

from argparse import ArgumentParser, FileType
from borg.logger import setup_logging
from borg.repository import LoggedIO

parser = ArgumentParser()
parser.add_argument('-r', '--repository', help='Borg repository on which to perform recovery', type=str, required=True)
parser.add_argument('-s', '--segment', help='Segment that has suffered a checksum mismatch', type=int, required=True)
args = parser.parse_args()

# Adapted from https://github.com/borgbackup/borg/issues/5182#issuecomment-628995211
MAX_SEGMENT_SIZE=524288000
SEGMENTS_PER_DIR=100

setup_logging()
lo = LoggedIO(args.repository, MAX_SEGMENT_SIZE, SEGMENTS_PER_DIR)
lo.recover_segment(args.segment, lo.segment_filename(args.segment))
