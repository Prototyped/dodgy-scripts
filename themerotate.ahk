EnvGet, themes, LocalAppData
themes .= "\Microsoft\Windows\Themes\*.theme"
theme_files := Array()
Loop, Files, %themes%, R
	theme_files.Push(A_LoopFilePath)
len := theme_files.Length()
Random, theme_num, 1, len

theme_file := theme_files[theme_num]

try themeManager := ComObjCreate(CLSID_IThemeManager := "{C04B329E-5823-4415-9C93-BA44688947B0}", IID_IThemeManager := "{0646EBBE-C1B7-4045-8FD0-FFD65D3FC792}")	
if (themeManager) {
	themeBstr := DllCall("oleaut32\SysAllocString", "WStr", theme_file, "Ptr")
	if (themeBstr) {
		DllCall(NumGet(NumGet(themeManager+0)+4*A_PtrSize), "Ptr", themeManager, "Ptr", themeBstr) ; ::ApplyTheme
		DllCall("oleaut32\SysFreeString", "Ptr", themeBstr)
	}
	ObjRelease(themeManager)
}
