#!/usr/bin/env python3

import json
import os
from pathlib import Path
import re
import subprocess
import sys
from urllib.request import urlcleanup, urlopen, Request


def get_installed_digest(repo: str, tag: str) -> str:
    content = subprocess.check_output([
        'docker', 'image', 'inspect', f'{repo}:{tag}',
    ], stderr=subprocess.STDOUT).decode('UTF-8')
    obj = json.loads(content)
    return obj[0]['Id']


def pull_container(repo: str, tag: str):
    try:
        subprocess.check_output([
            'docker', 'pull', f'{repo}:{tag}',
        ], stderr=subprocess.STDOUT).decode('UTF-8')
    except subprocess.CalledProcessError as exc:
        print(exc.output.decode('UTF-8'))
        raise


def container_updated(repo: str, tag: str) -> bool:
    orig = get_installed_digest(repo, tag)
    pull_container(repo, tag)
    new = get_installed_digest(repo, tag)
    return orig != new

if __name__ == '__main__':
    sys.exit(
        1 if container_updated(
            'jlesage/crashplan-pro',
            'latest')
        else 0)
