#!/bin/bash

set -euo pipefail

exec 3>&2 1>>/var/log/borg-backup.log 2>&1

trap 'cleanup "$?" "$LINENO"' EXIT

cleanup_snapshot() {
    set +e
    umount /mnt/aux2
    lvremove -f Data/backup-snapshot
    set -e
}

cleanup_mount() {
    set +e
    umount /mnt/backup
    set -e
}

cleanup() {
    local exit_code="$1"
    local line_number="$2"
    
    set +e

    if [[ "$exit_code" -ne 0 ]]
    then
        echo Backup script failed at line number $line_number with exit code $exit_code. See below for details. 1>&3
        cat /var/log/borg-backup.log 1>&3
    fi

    cleanup_snapshot
    cleanup_mount
}

PATH="${PATH}:/sbin:/usr/sbin"

[ -x /etc/init.d/lvm2-lvmetad ] && /etc/init.d/lvm2-lvmetad restart

[ -x /etc/init.d/lvm2-lvmpolld ] && /etc/init.d/lvm2-lvmpolld restart

[ -x /etc/init.d/lvm2 ] && /etc/init.d/lvm2 restart

. /etc/borg/passphrase
export BORG_PASSPHRASE

[ -d /mnt/backup/data ] || mount /mnt/backup

[ -d /mnt/aux2/home ] && umount -f /mnt/aux2

[ -e /dev/Data/backup-snapshot ] && lvremove -f /dev/Data/backup-snapshot


sync
sync
sync

lvm_snapshot() {
    local volume="$1"
    lvcreate -L224GiB -s -n backup-snapshot "$volume"
    mount -o ro,nouuid /dev/Data/backup-snapshot /mnt/aux2
}

btrfs_snapshot() {
    local volume="$1"
    local snapshot_parent="$2"

    mkdir -p $snapshot_parent
    snapshot_dir="$snapshot_parent/$id"
    btrfs subvolume snapshot "$volume" "$snapshot_dir"

    mount --bind "$snapshot_dir" /mnt/aux2
}

cleanup_btrfs_snapshot() {
    set +e
    umount /mnt/aux2
    btrfs subvolume delete "$snapshot_dir"
    set -e
}

id="$(date -u '+%Y-%m-%dT%H:%M:%SZ')"

if [ "${1-ssdroot}" = ssdroot ]; then

    lvm_snapshot /dev/Data/Root

    time borg create -x --numeric-ids -C lz4 -e '/mnt/aux2/tmp' -e '/mnt/aux2/var/tmp' -e '/mnt/aux2/var/cache' -e '/mnt/aux2/SWAPFILE' -s -p "/mnt/backup/ssdroot::$id" /mnt/aux2/.
    cleanup_snapshot

    time borg prune -d 7 -w 9 -m 12 -y 2 /mnt/backup/ssdroot
    time borg compact /mnt/backup/ssdroot
fi

if [ "${1-data}" = data ]; then

    lvm_snapshot /dev/Data/data

    time borg create -x --numeric-ids -C lz4 -s -p "/mnt/backup/data::$id" /mnt/aux2/.
    cleanup_snapshot

    time borg prune -d 7 -w 9 -m 12 -y 2 /mnt/backup/data
    time borg compact /mnt/backup/data
fi

cleanup_mount
