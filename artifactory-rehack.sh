#!/bin/bash

set -euo pipefail

! [[ -x /opt/jfrog/artifactory/app/metadata/bin/metadata.sh ]] && {
    echo "/opt/jfrog/artifactory/app/metadata/bin/metadata.sh not found, cannot patch for x86_64 -> aarch64"
    exit 1
}

sed -ir "s/x86_64/aarch64/g" /opt/jfrog/artifactory/app/metadata/bin/metadata.sh
systemctl restart artifactory
