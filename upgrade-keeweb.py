#!/usr/bin/env python3

from html.parser import HTMLParser
import os
from pathlib import Path
import re
import subprocess
import sys
from typing import Tuple
from urllib.request import urlcleanup, urlopen, urlretrieve


class ReleaseFilter(HTMLParser):
    def __init__(self, pattern: re.Pattern):
        super().__init__()
        self.note = ''
        self.artifact_url = None
        self.release = None
        self.nested_div_count = 0
        self.pattern = pattern
        self.parent_a_attrs = None

    def in_note(self):
        return self.nested_div_count > 0

    def handle_data(self, data):
        if self.in_note():
            self.note += data

    def handle_starttag(self, tag, attrs):
        lower_tag = tag.lower()
        if not self.artifact_url and lower_tag == 'a':
            for attr, value in attrs:
                if attr.lower() == 'href' and self.pattern.search(value):
                    self.artifact_url = 'https://github.com' + value
                    break

        if not self.release:
            if lower_tag == 'a':
                self.parent_a_attrs = attrs
            elif lower_tag == 'svg' and self.parent_a_attrs:
                for attr, value in attrs:
                    if attr.lower() == 'class' and 'octicon-tag' in value:
                        for parent_attr, parent_value in self.parent_a_attrs:
                            if parent_attr.lower() == 'title':
                                self.release = parent_value
                                break
                        break

        if lower_tag == 'div':
            if self.in_note():
                self.nested_div_count += 1
            else:
                for attr, value in attrs:
                    if attr.lower() == 'class' and 'markdown-body' in value:
                        self.nested_div_count += 1
                        break

    def handle_endtag(self, tag):
        lower_tag = tag.lower()
        if self.release and lower_tag == 'a':
            self.parent_a_attrs = None

        if lower_tag == 'div' and self.in_note():
            self.nested_div_count -= 1


def get_latest_release(repo: str, pattern: re.Pattern) -> Tuple[str, str, str]:
    tag_url = f'https://github.com/{repo}/releases/latest'
    with urlopen(tag_url) as tag_response:
        if tag_response.status < 200 or tag_response.status >= 300:
            raise RuntimeError(f'Unexpected status {tag_response.status} attempting to obtain latest release page for repository {repo}: {tag_response.reason}')

        content = tag_response.read().decode('UTF-8')
        filter = ReleaseFilter(pattern)
        filter.feed(content)

        if not filter.release:
            raise RuntimeError(f'release not found on tag page {tag_url} for repository {repo}')


        if not filter.artifact_url:
            raise RuntimeError(f'artifact URL not found on tag page {tag_url} for repository {repo}, release {filter.release}')

        if not filter.note:
            raise RuntimeError(f'release notes not found on tag page {tag_url} for repository {repo}, release {filter.release}')

        return filter.release, filter.artifact_url, filter.note


def get_installed_version(package: str) -> str:
    try:
        return subprocess.check_output([
            'dpkg-query', '-f', '${Version}', '-W', package,
        ], stderr=subprocess.STDOUT).decode('UTF-8')
    except subprocess.CalledProcessError as exc:
        print(f'Failed to obtain installed version of {package}: {exc.output.decode("UTF-8")}')
        # and swallow
        return None


def download_artifact(repo: str, tag: str, artifact_url: str, destination: str) -> Path:
    try:
        urlretrieve(artifact_url, destination)
        return destination
    finally:
        urlcleanup()


def install_deb(repo: str, tag: str, deb_path: str):
    try:
        return subprocess.check_output([
            'dpkg', '-i', deb_path,
        ], stderr=subprocess.STDOUT).decode('UTF-8')
    except subprocess.CalledProcessError as exc:
        raise RuntimeError(f'Failed to install deb {deb_path}: {exc.output.decode("UTF-8")}') from exc

if __name__ == '__main__':
    repo = 'keeweb/keeweb'
    package = 'keeweb-desktop'
    artifact_pattern = re.compile(r'\.linux\.x64\.deb')
    release, artifact_url, notes = get_latest_release(repo, artifact_pattern)
    if release.startswith('v'):
        release = release[1:]
    destination = Path('/tmp', Path(artifact_url).name)
    installed_version = get_installed_version('keeweb-desktop')
    if not installed_version or not installed_version.startswith(release):
        print(f'release: {release}, artifact URL: {artifact_url}\nnotes:\n\n{notes}')
        artifact_path = download_artifact(repo, release, artifact_url, destination)
        print(f'Downloaded artifact from {artifact_url} to {artifact_path}')
        try:
            install_output = install_deb(repo, release, artifact_path)
            print(f'Installed deb!\n{install_output}')
        finally:
            os.unlink(artifact_path)
    else:
        if sys.stdout.isatty():
            print(f'{package} is already at the latest release {release} -- installed version is {installed_version}')
