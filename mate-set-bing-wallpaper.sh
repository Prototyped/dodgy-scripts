#!/bin/sh
set -eu

state_dir="$HOME/.local/share/mate-set-bing-wallpaper"
mkdir -p "$state_dir"

if ! [ -r "$state_dir/b" ]
then
    old_state="$state_dir/a"
    new_state="$state_dir/b"
    locale=en-GB
    xml_url="http://www.bing.com/HPImageArchive.aspx?format=xml&idx=1&n=1&mkt=$locale"
    image_url="https://www.bing.com$(curl -fLSs "$xml_url" |
        xmllint --quiet --xpath '//url/text()' - |
        sed 's/&amp;/&/g')"
else
    old_state="$state_dir/b"
    new_state="$state_dir/a"
    image_url="https://www.bing.com$(curl '-HUser-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0' -fLSs https://www.bing.com |
        sed -r '/background-image: url\(&quot;/!d; s|^.*background-image: url\(&quot;/th|/th|; s/&quot;.+$//; s/&amp;/\&/g')"
fi
    
image_filename="${image_url##*\?}"
image_filename="${image_filename%%\&*}"
download_directory="$HOME/.local/share/org.keshavnrj.ubuntu/BingWall/downloaded"
downloaded_wallpaper="$download_directory/$image_filename"
curl -fLSso "$downloaded_wallpaper" "$image_url"

gsettings set org.gnome.desktop.background picture-uri "file://$downloaded_wallpaper"
gsettings set org.mate.background picture-filename "$downloaded_wallpaper"
sed -ri 's|^#?[[:space:]]*image =.+$|image = '"$downloaded_wallpaper|" "$HOME/.config/wf-shell.ini"

rm -f "$old_state"
touch "$new_state"
