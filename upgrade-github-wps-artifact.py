#!/usr/bin/env python3

from argparse import ArgumentParser
import base64
from collections import ChainMap, namedtuple
from html.parser import HTMLParser
from http.client import HTTPConnection
import json
import os
from pathlib import Path
import re
from shutil import copyfileobj
import subprocess
import sys
from typing import Tuple, Dict, List
from urllib.request import build_opener, urlcleanup, urlopen, Request, HTTPCookieProcessor, HTTPRedirectHandler
from urllib.error import HTTPError


class GitHubExpandedAssetsFilter(HTMLParser):
    def __init__(self, pattern: re.Pattern):
        super().__init__()
        self.artifact_url = None
        self.pattern = pattern

    def handle_starttag(self, tag, attrs):
        lower_tag = tag.lower()
        if not self.artifact_url and lower_tag == 'a':
            for attr, value in attrs:
                if attr.lower() == 'href' and self.pattern.search(value):
                    self.artifact_url = 'https://github.com' + value
                    break


class GitHubReleaseFilter(HTMLParser):
    def __init__(self):
        super().__init__()
        self.note = ''
        self.nested_div_count = 0
        self.list_tags = []
        self.ordinal = 0
        self.following_data = False

    def in_note(self):
        return self.nested_div_count > 0

    def handle_data(self, data):
        if self.in_note():
            if self.following_data:
                self.note += ' ' + data.strip()
            else:
                self.note += data.strip()
        self.following_data = True

    def handle_starttag(self, tag, attrs):
        lower_tag = tag.lower()
        if lower_tag == 'div':
            self.following_data = False
            if self.in_note():
                self.nested_div_count += 1
            else:
                for attr, value in attrs:
                    if attr.lower() == 'class' and 'markdown-body' in value:
                        self.nested_div_count += 1
                        break

        if self.in_note() and lower_tag in ('ol', 'ul'):
            self.following_data = False
            self.list_tags.append(lower_tag)

        if self.in_note() and lower_tag == 'li' and self.list_tags:
            self.following_data = False
            last_list = self.list_tags[-1]
            indent = '  ' * len(self.list_tags)
            if last_list == 'ul':
                self.note += indent + "- "
            else:
                self.ordinal += 1
                self.note += indent + str(self.ordinal) + '. '

        if self.in_note() and lower_tag == 'br':
            self.note += '\n'
            self.following_data = False

        if self.in_note() and lower_tag == 'p':
            self.following_data = False

    def handle_endtag(self, tag):
        lower_tag = tag.lower()

        if lower_tag == 'div' and self.in_note():
            self.nested_div_count -= 1

        if lower_tag in ('p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6') and self.in_note():
            self.note += '\n\n'

        if lower_tag in ('div', 'li') and self.in_note():
            self.note += '\n'

        if lower_tag in ('ol', 'ul') and self.in_note():
            self.list_tags.pop()
            if lower_tag == 'ol':
                self.ordinal = 0

        self.following_data = self.in_note() and lower_tag in ('code', 'strong', 'em', 'a')


def parse_args(prog: str, *args: str) -> object:
    parser = ArgumentParser(prog=prog, description='Update non-apt artifacts')
    parser.add_argument('--wps-rpm', action='store_true', help='Update WPS Office .rpm release')
    parser.add_argument('--wps', action='store_true', help='Update WPS Office .deb release')
    parser.add_argument('--deb', action='store', help='If GitHub release is a .deb, name of package')
    parser.add_argument('--rpm', action='store', help='If GitHub release is an .rpm, name of package')
    parser.add_argument('--github', action='store', help='Update GitHub release from specified repo')
    parser.add_argument('--url', action='store', help='HTTP(S) URL to .deb')
    parser.add_argument('--destination', action='store', help='Where to write the GitHub release executable')
    parser.add_argument('--release-pattern', action='store', help='Regular expression pattern to match release artifact')
    parser.add_argument('--version-arg', action='store', help='Argument to get the version string from a GitHub release executable')
    parsed_args = parser.parse_args(*args)

    source = 0
    source += 1 if parsed_args.wps else 0
    source += 1 if parsed_args.wps_rpm else 0
    source += 1 if parsed_args.github else 0
    source += 1 if parsed_args.url else 0

    if source > 1:
        parser.print_help()
        raise ValueError('Specify either --wps/--wps-rpm or --url or --github=repo --destination=/usr/local/bin/something, not more than one')
    elif source < 1:
        parser.print_help()
        raise ValueError('Specify exactly one of --wps, --wps-rpm, --url or --github=repo.')

    if parsed_args.github or parsed_args.url:
        pkg = 0
        pkg += 1 if parsed_args.deb else 0
        pkg += 1 if parsed_args.rpm else 0
        pkg += 1 if parsed_args.destination else 0

        if pkg != 1:
            parser.print_help()
            raise ValueError('--github or --url must be accompanied by either --deb/--rpm or --destination --version-arg, not both')

        if parsed_args.destination and not parsed_args.version_arg:
            parser.print_help()
            raise ValueError('--destination must be accompanied by --version-arg')

    if (parsed_args.deb or parsed_args.version_arg or parsed_args.release_pattern or parsed_args.destination) and not parsed_args.github and not parsed_args.url:
        parser.print_help()
        raise ValueError('--deb/--rpm, --version-arg, --destination and --release-pattern are only valid with --github or --url')

    return parsed_args

WPS_PACKAGE_VERSION_RE=re.compile(r'wps-office_(?P<version>.+?)_')
def get_wps_latest_release(name: str, is_deb=True) -> Tuple[str, str, str]:
    try:
        wps_url = 'https://params.wps.com/api/map/web/newwpsapk?pttoken=newlinuxpackages'
        with urlopen(wps_url) as wps_response:
            if wps_response.status < 200 or wps_response.status >= 300:
                raise RuntimeError(f'Unexpected status {wps_response.status} attempting to load WPS Office Linux packages API {wps_url}: {wps_response.reason}')

            outer_json_content = json.loads(wps_response.read().decode('UTF-8'))
            encoded_inner_json = outer_json_content['staticjs']['website']['wpsnewpackages']['downloads']
            
            inner_json = base64.b64decode(encoded_inner_json).decode('UTF-8')
            js_content = json.loads(inner_json)

            if is_deb:
                pkg_url = js_content['linux_deb']
            else:
                pkg_url = js_content['linux_rpm']

            matcher = WPS_PACKAGE_VERSION_RE.search(pkg_url)
            if matcher:
                release = matcher.group('version')
            if not matcher or not release:
                raise RuntimeError(f'release not found in WPS Office package URL {pkg_url}.')

            return release, pkg_url, pkg_url
    except HTTPError as exc:
        raise RuntimeError(f'Unable to load URL {last_url}') from exc


def get_github_latest_release(repo: str, pattern: re.Pattern) -> Tuple[str, str, str]:
    tag_url = f'https://github.com/{repo}/releases/latest'
    with urlopen(tag_url) as tag_response:
        if tag_response.status < 200 or tag_response.status >= 300:
            raise RuntimeError(f'Unexpected status {tag_response.status} attempting to obtain latest release page for repository {repo}: {tag_response.reason}')

        redirected_url = tag_response.geturl()
        if not redirected_url:
            raise RuntimeError(f'Redirect not found in request for tag page {tag_url} for repository {repo}: {tag_response.headers}')

        url_pattern = re.compile(r'/releases/tag/(?P<tag>.+)$')
        matcher = url_pattern.search(tag_response.geturl())
        if matcher:
            release = matcher.group('tag')
        if not matcher or not release:
            raise RuntimeError(f'release not found in tag URL {tag_url} for repository {repo}')

        content = tag_response.read().decode('UTF-8')
        tag_filter = GitHubReleaseFilter()
        tag_filter.feed(content)

        if not tag_filter.note:
            raise RuntimeError(f'release notes not found on tag page {tag_url} for repository {repo}, release {release}')

        expanded_assets_url = f'https://github.com/{repo}/releases/expanded_assets/{release}'
        with urlopen(expanded_assets_url) as expanded_assets_response:
            content = expanded_assets_response.read().decode('UTF-8')
            expanded_assets_filter = GitHubExpandedAssetsFilter(pattern)
            expanded_assets_filter.feed(content)

            if not expanded_assets_filter.artifact_url:
                raise RuntimeError(f'artifact URL not found on tag page {tag_url} for repository {repo}, release {release}')

        return release, expanded_assets_filter.artifact_url, tag_filter.note


def get_exe_installed_version(exe: str, version_arg: str) -> str:
    try:
        return subprocess.check_output([
            exe, version_arg
        ], stderr=subprocess.STDOUT).decode('UTF-8').rstrip()
    except FileNotFoundError as exc:
        print(f'Could not find {exe}')
        # and swallow
        return None
    except subprocess.CalledProcessError as exc:
        print(f'Failed to obtain installed version of {exe}: {exc.output.decode("UTF-8")}')
        # and swallow
        return None


def get_deb_installed_version(package: str) -> str:
    try:
        return subprocess.check_output([
            'dpkg-query', '-f', '${Version}', '-W', package,
        ], stderr=subprocess.STDOUT).decode('UTF-8')
    except subprocess.CalledProcessError as exc:
        print(f'Failed to obtain installed version of {package}: {exc.output.decode("UTF-8")}')
        # and swallow
        return None


def get_rpm_installed_version(package: str) -> str:
    try:
        return subprocess.check_output([
            'rpm', '--qf', '%{Version}', '-q', package,
        ], stderr=subprocess.STDOUT).decode('UTF-8')
    except subprocess.CalledProcessError as exc:
        print(f'Failed to obtain installed version of {package}: {exc.output.decode("UTF-8")}')
        # and swallow
        return None


def download_artifact(artifact_url: str, destination: str, mode: int) -> Path:
    try:
        request = Request(artifact_url, headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0',
        })
        with urlopen(request) as file_stream, open(destination, 'wb') as out_file:
            copyfileobj(file_stream, out_file)
        os.chmod(destination, mode)
        return destination
    finally:
        urlcleanup()


def install_deb(deb_path: str):
    try:
        return subprocess.check_output([
            'dpkg', '-i', deb_path,
        ], stderr=subprocess.STDOUT,
        env=ChainMap({
            'PATH': f'{os.environ["PATH"]}:/sbin:/usr/sbin',
        }, os.environ)).decode('UTF-8')
    except subprocess.CalledProcessError as exc:
        raise RuntimeError(f'Failed to install deb {deb_path}: {exc.output.decode("UTF-8")}') from exc


def install_rpm(rpm_path: str):
    try:
        return subprocess.check_output([
            'rpm', '-U', rpm_path,
        ], stderr=subprocess.STDOUT,
        env=ChainMap({
            'PATH': f'{os.environ["PATH"]}:/sbin:/usr/sbin',
        }, os.environ)).decode('UTF-8')
    except subprocess.CalledProcessError as exc:
        raise RuntimeError(f'Failed to install rpm {rpm_path}: {exc.output.decode("UTF-8")}') from exc


def download_and_install_artifact(installed_version: str, release: str, artifact_url: str, destination: str, notes: str, deb: bool, rpm: bool, mode: int, package: str):
    if not installed_version or not release or (not installed_version.startswith(release) and not installed_version.endswith(release)):
        print(f'release: {release}, artifact URL: {artifact_url}\nnotes:\n\n{notes}')
        artifact_path = download_artifact(artifact_url, destination, 0o644 if deb else 0o755)
        print(f'Downloaded artifact from {artifact_url} to {artifact_path}')
        if deb:
            try:
                install_output = install_deb(artifact_path)
                print(f'Installed deb!\n{install_output}')
            finally:
                os.unlink(artifact_path)
        elif rpm:
            try:
                install_output = install_rpm(artifact_path)
                print(f'Installed rpm!\n{install_output}')
            finally:
                os.unlink(artifact_path)
    else:
        if sys.stdout.isatty():
            print(f'{package} is already at the latest release {release} -- installed version is {installed_version}')


def update_wps(is_deb=True):
    name = 'WPS Office'
    package = 'wps-office'
    release, artifact_url, notes = get_wps_latest_release(name, is_deb=is_deb)
    if release.startswith('v'):
        release = release[1:]
    destination = Path('/tmp', Path(artifact_url).name)
    if is_deb:
        installed_version = get_deb_installed_version(package)
    else:
        installed_version = get_rpm_installed_version(package)
    download_and_install_artifact(installed_version, release, artifact_url, destination, notes, is_deb, not is_deb, 0o644, package)


def update_github_release(repo: str, release_pattern: str, destination: str, version_arg: str, deb: str, rpm: str):
    artifact_pattern = re.compile(release_pattern)
    release, artifact_url, notes = get_github_latest_release(repo, artifact_pattern)
    if release.startswith('v'):
        release = release[1:]
    if deb:
        destination = Path('/tmp', Path(artifact_url).name)
        installed_version = get_deb_installed_version(deb)
    elif rpm:
        destination = Path('/tmp', Path(artifact_url).name)
        installed_version = get_rpm_installed_version(rpm)
    else:
        installed_version = get_exe_installed_version(destination, version_arg)
    download_and_install_artifact(installed_version, release, artifact_url, destination, notes, deb, rpm, 0o644 if deb or rpm else 0o755, repo)


def update_from_url(artifact_url: str, destination: str, version_arg: str, deb: str, rpm: str, release=None, notes=''):
    if deb:
        destination = Path('/tmp', Path(artifact_url).name)
        installed_version = get_deb_installed_version(deb)
    elif rpm:
        destination = Path('/tmp', Path(artifact_url).name)
        installed_version = get_rpm_installed_version(rpm)
    else:
        installed_version = get_exe_installed_version(destination, version_arg)
    download_and_install_artifact(installed_version, release, artifact_url, destination, notes, deb, 0o644 if deb or rpm else 0o755, artifact_url, deb if deb is not None else rpm)

if __name__ == '__main__':
    parsed_args = parse_args(sys.argv[0], sys.argv[1:])
    if parsed_args.wps or parsed_args.wps_rpm:
        update_wps(is_deb=(True if parsed_args.wps else False))
    elif parsed_args.github:
        update_github_release(parsed_args.github, parsed_args.release_pattern, parsed_args.destination, parsed_args.version_arg, parsed_args.deb, parsed_args.rpm)
    else:
        update_from_url(parsed_args.url, parsed_args.destination, parsed_args.version_arg, parsed_args.deb, parsed_args.rpm)

