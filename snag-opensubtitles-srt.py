#!/usr/bin/env python3

from html.parser import HTMLParser
import re
import sys
import time
from urllib.request import Request, urlcleanup, urlopen, urlretrieve


EPISODE_URLS = [
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539741',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539822',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539916',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539818',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539770',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539786',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539852',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539697',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539684',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539832',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539758',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539773',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539737',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539888',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539766',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539925',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539892',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539707',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539857',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539914',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539898',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539899',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539676',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539768',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539913',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539910',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539726',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539751',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539919',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-789378',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539745',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539844',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539849',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539924',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539748',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539749',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539934',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539853',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539781',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539747',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539698',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539801',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539891',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539807',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539721',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539722',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539798',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539710',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539837',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539927',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539802',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539759',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539706',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-293031',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539694',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539922',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539815',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539932',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539765',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539683',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539889',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539923',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539777',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539845',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539720',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539785',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539711',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539708',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539831',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539835',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539763',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539920',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539692',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539755',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539804',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539906',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539803',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539921',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539699',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539926',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539760',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539679',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539778',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539757',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539738',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539814',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539809',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539823',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539677',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539792',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539843',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539859',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539871',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539838',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539700',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539761',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539688',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539903',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539880',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539900',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539904',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539734',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539875',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539893',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539780',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539746',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539723',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539799',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539868',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539816',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539784',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539796',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539782',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539877',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539902',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539701',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539797',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539930',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539783',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539836',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539896',
    'https://www.opensubtitles.org/en/search/sublanguageid-eng/imdbid-539826',
]

FIREFOX_USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:133.0) Gecko/20100101 Firefox/133.0'

SUB_ID_PATTERN = re.compile(r'/en/subtitles/(\d+)/.+-en')

DOWNLOAD_URL_PATTERN = re.compile(r'https://www.opensubtitles.com/download/.+\.srt')


class SubtitlesFilter(HTMLParser):
    EPISODE_PREFIX=re.compile(r'^.+\n\t\t')
    def __init__(self):
        super().__init__()
        self.in_search_results = False
        self.table_count = 0
        self.td_count = 0
        self.href = None
        self.in_br = False
        self.name = None
        self.subtitles = []

    def handle_starttag(self, tag, attrs):
        lower_tag = tag.lower()
        if self.in_search_results:
            if lower_tag == 'tr':
                self.td_count = 0
            elif lower_tag == 'td':
                self.td_count += 1
            elif self.td_count == 1:
                if lower_tag == 'a':
                    for attr, value in attrs:
                        lower_attr = attr.lower()
                        if lower_attr == 'href' and not self.href:
                            self.href = value
                            break
                elif lower_tag == 'br':
                    self.in_br = True
                elif lower_tag == 'span':
                    for attr, value in attrs:
                        lower_attr = attr.lower()
                        if lower_attr == 'title':
                            self.name = value.strip()
                            break
                elif self.in_br:
                    self.in_br = False
        elif lower_tag == 'table':
            self.table_count += 1
            for attr, value in attrs:
                lower_attr = attr.lower()
                if lower_attr == 'id' and value == 'search_results':
                    self.in_search_results = True
                    break

    def handle_endtag(self, tag):
        lower_tag = tag.lower()
        if self.in_search_results:
            if lower_tag == 'table':
                if self.table_count == 1:
                    self.in_search_results = False
                self.table_count -= 1
            elif lower_tag == 'tr':
                if self.name and self.href:
                    self.subtitles.append((self.name, self.href))
                self.href = None
                self.name = None

    def handle_data(self, data):
        if self.td_count == 1 and self.in_br and not self.name:
            self.name = re.sub(self.EPISODE_PREFIX, '', data.strip())

    def get_subtitles(self):
        return self.subtitles


class DownloadMetadataFilter(HTMLParser):
    def __init__(self):
        super().__init__()
        self.section_depth = 0
        self.in_subtitle_actions = False
        self.download_metadata = None

    def handle_starttag(self, tag, attrs):
        lower_tag = tag.lower()
        if self.download_metadata:
            return
        elif tag == 'section':
            self.section_depth += 1
            if self.section_depth == 3:
                for attr, value in attrs:
                    lower_attr = attr.lower()
                    if lower_attr == 'id' and value == 'subtitle-actions':
                        self.in_subtitle_actions = True
                        break
        elif self.in_subtitle_actions:
            if lower_tag == 'a':
                is_download = False
                sub_name = None
                metadata_url = None
                for attr, value in attrs:
                    lower_attr = attr.lower()
                    if lower_attr == 'download':
                        is_download = True
                        sub_name = value
                    elif lower_attr == 'href':
                        metadata_url = value
                if is_download:
                    self.download_metadata = (sub_name, metadata_url)

    def handle_endtag(self, tag):
        lower_tag = tag.lower()
        if tag == 'section':
            self.section_depth -= 1
            self.in_subtitle_actions = False

    def get_download_metadata(self):
        return self.download_metadata


def search_subs(search_url):
    with urlopen(search_url) as search_response:
        if search_response.status < 200 or search_response.status >= 300:
            raise RuntimeError(f'Unexpected status {search_response.status} attempting to search for subtitles for URL {search_url}: {search_response.reason}')

        content = search_response.read().decode('UTF-8')
        filter = SubtitlesFilter()
        filter.feed(content)

        return filter.get_subtitles()


def select_sub(subs):
    ntb = None
    troll_hd = None
    dvd_rip_non_hi = None
    dvd_rip_hi = None

    for sub in subs:
        name = sub[0].lower()
        url = sub[1]
        if name.endswith('ntb'):
            ntb = sub
        elif '1080p.web-dl' in name:
            troll_hd = sub
        elif 'dvdrip.nonhi' in name:
            dvd_rip_non_hi = sub
        elif 'dvdrip.hi' in name:
            dvd_rip_hi = sub

    if troll_hd:
        return troll_hd
    elif ntb:
        return ntb
    elif dvd_rip_non_hi:
        return dvd_rip_non_hi
    elif dvd_rip_hi:
        return dvd_rip_hi
    else:
        return None


def download_sub(name, relative_url, search_url, cookie):
    sub_id_matcher = SUB_ID_PATTERN.match(relative_url)
    if not sub_id_matcher:
        raise ValueError(f"Expected relative sub download page URL {relative_url} to have a sub ID but couldn't find it.")
    sub_id = sub_id_matcher.group(1)
    if not sub_id:
        raise ValueError(f"Expected relative sub download page URL {relative_url} to have a sub ID but couldn't find it.")
    sub_url = f"https://www.opensubtitles.com/en/subtitles/legacy/{sub_id}/download"
    sub_headers = {
        'User-Agent': FIREFOX_USER_AGENT,
        'Referer': search_url,
    }
    if cookie:
        sub_headers['Cookie'] = cookie
    sub_request = Request(sub_url, headers=sub_headers)

    with urlopen(sub_request) as sub_response:
        if sub_response.status < 200 or sub_response.status >= 300:
            raise RuntimeError(f'Unexpected status {sub_response.status} attempting to load sub download page for sub {name} at URL {sub_url}: {sub_response.reason}')

        content = sub_response.read().decode('UTF-8')

        filter = DownloadMetadataFilter()
        filter.feed(content)

        download_metadata = filter.get_download_metadata()

        metadata_name = download_metadata[0]
        metadata_url = download_metadata[1]
        metadata_headers = {
            'User-Agent': FIREFOX_USER_AGENT,
            'Referer': sub_response.url,
        }
        if cookie:
            metadata_headers['Cookie'] = cookie
        metadata_request = Request(f"https://www.opensubtitles.com{metadata_url}", headers=metadata_headers)

        with urlopen(metadata_request) as metadata_response:
            if metadata_response.status < 200 or metadata_response.status >= 300:
                raise RuntimeError(f'Unexpected status {metadata_response.status} attempting to obtain metadata in JavaScript snippet for sub {metadata_name} at URL {metadata_url}: {metadata_response.reason}')

            content = metadata_response.read().decode('UTF-8')

            matches = DOWNLOAD_URL_PATTERN.search(content)
            if not matches:
                raise ValueError(f"Expected metadata content to have the download URL, for sub {name} at URL {sub_response.url}. Content:\n{content}")

            download_url = matches.group(0)

            if not download_url:
                raise ValueError(f"Expected metadata content to have the download URL, for sub {name} at URL {sub_response.url}. Content:\n{content}")

            download_headers = {
                'User-Agent': FIREFOX_USER_AGENT,
                'Referer': metadata_response.url,
            }
            if cookie:
                download_headers['Cookie'] = cookie
            download_request = Request(download_url, headers=download_headers)
            with urlopen(download_request) as download_response:
                if download_response.status < 200 or download_response.status >= 300:
                    raise RuntimeError(f'Unexpected status {download_response.status} attempting to download subtitle {metadata_name} at URL {download_url}: {download_response.reason}')
                with open(metadata_name, 'wb') as sub:
                    sub.write(download_response.read())

                print(f"{metadata_name} corresponding to {search_url} downloaded.")


def download_sub_for_title(search_url, cookie):
    subs = search_subs(search_url)
    preferred_sub = select_sub(subs)
    if preferred_sub:
        download_sub(preferred_sub[0], preferred_sub[1], search_url, cookie)
    else:
        raise ValueError(f"Didn't find any suitable sub out of {', '.join(subs)}.")
    time.sleep(0.2)


def download_all_subs():
    cookie = None
    if len(sys.argv) > 1:
        cookie = sys.argv[1]
    for search_url in EPISODE_URLS:
        download_sub_for_title(search_url, cookie)


if __name__ == "__main__":
    download_all_subs()
    
